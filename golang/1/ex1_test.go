package ex1_test

import (
  "testing"
  ex1 "bitbucket.org/silversupreme/euler/golang/1"
)

func TestExercise(t *testing.T) {
  if answer := ex1.Exercise(); answer != 233168 {
    t.Fatalf("Got %d, expected 233168!", answer)
  }
}
